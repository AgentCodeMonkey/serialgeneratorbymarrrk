﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Security.Cryptography;
using System.Text.RegularExpressions;

namespace SerialNumberGenerator
{
	public class Serial
	{
		private class Entry
		{
			public object Value;
			public int Size;
			public Type Type;
		}

		private Dictionary<string, Entry> featureMap = new Dictionary<string, Entry>();

		public byte MinFeatureByteCount = 20;
		public byte DelimiterCount = 4;

		public int GetIntFeature(string name)
		{
			return (int)featureMap[name].Value;
		}

		public byte GetByteFeature(string name)
		{
			return (byte)featureMap[name].Value;
		}

		public float GetFloatFeature(string name)
		{
			return (float)featureMap[name].Value;
		}

		public bool GetBoolFeature(string name)
		{
			return (bool)featureMap[name].Value;
		}

		public string GetStringFeature(string name)
		{
			return (string)featureMap[name].Value;
		}

		public DateTime GetDateFeature(string name)
		{
			return (DateTime)featureMap[name].Value;
		}

		public void AddByteFeature(string name)
		{
			SetByteFeature(name, default(byte));
		}

		public void AddIntFeature(string name)
		{
			SetIntFeature(name, default(int));
		}

		public void AddFloatFeature(string name)
		{
			SetFloatFeature(name, default(float));
		}

		public void AddStringFeature(string name, int maxLength)
		{
			SetStringFeature(name, default(string), maxLength);
		}

		public void AddDateFeature(string name)
		{
			SetDateFeature(name, default(DateTime));
		}

		public void AddBoolFeature(string name)
		{
			SetBoolFeature(name, default(bool));
		}

		public void SetByteFeature(string name, byte value)
		{
			featureMap[name] = new Entry() { Value = value, Type = typeof(byte) };
		}

		public void SetIntFeature(string name, int value)
		{
			featureMap[name] = new Entry() { Value = value, Type = typeof(int) };
		}

		public void SetFloatFeature(string name, float value)
		{
			featureMap[name] = new Entry() { Value = value, Type = typeof(float) };
		}

		public void SetStringFeature(string name, string value, int maxLength)
		{
			featureMap[name] = new Entry() { Value = value, Type = typeof(string), Size = maxLength };
		}

		public void SetDateFeature(string name, DateTime value)
		{
			featureMap[name] = new Entry() { Value = value, Type = typeof(DateTime) };
		}

		public void SetBoolFeature(string name, bool value)
		{
			featureMap[name] = new Entry() { Value = value, Type = typeof(bool) };
		}

		public void RemoveFeature(string name)
		{
			featureMap.Remove(name);
		}

		public string Generate()
		{
			var bytes = new List<byte>();


			FillFeatureBytes(bytes);
			EncodeKeyBytes(bytes);

			var base32 = new Base32();
			var encoded = base32.Encode(bytes.ToArray()).ToUpper().TrimEnd();

			if (DelimiterCount > 0)
			{
				var distance = encoded.Length / DelimiterCount;
				if (distance > 0)
					encoded = Regex.Replace(encoded, ".{" + distance + "}", "$0-").TrimEnd('-');
			}
			return encoded;
		}

		public bool ReadKey(string key)
		{
			try
			{
				key = key.Replace("-", "").Trim().ToLower();

				var extra = (key.Length) % 5;
				if (extra != 0)
					extra = 5 - extra;
				for (int i = 0; i < extra; ++i)
					key += " ";

				var base32 = new Base32();
				var decoded = base32.Decode(key);

				var decodedBytes = decoded.ToList();
				if (!DecodeKeyBytes(decodedBytes))
					return false;
				if (!ExtractFeatures(decodedBytes))
					return false;
			}
			catch
			{
				return false;
			}

			return true;
		}

		private bool ExtractFeatures(List<byte> decoded)
		{
			AddByteFeature("_internalFeatureByteCount");

			var boolFeatures = featureMap.Values.Reverse().Where(entry => entry.Type == typeof(bool)).ToArray();
			var otherFeatures = featureMap.Values.Reverse().Where(entry => entry.Type != typeof(bool)).ToArray();
			var startFeatureByteCount = decoded.Count;

			for (int i = 0; i < otherFeatures.Length; ++i)
			{
				var feature = otherFeatures[i];
				if (feature.Type == typeof(int))
				{
					feature.Value = BitConverter.ToInt32(decoded.ToArray(), decoded.Count - 4);
					decoded.RemoveRange(decoded.Count - 4, 4);
				}
				else if (feature.Type == typeof(byte))
				{
					feature.Value = decoded[decoded.Count - 1];
					decoded.RemoveRange(decoded.Count - 1, 1);
				}
				else if (feature.Type == typeof(float))
				{
					feature.Value = BitConverter.ToSingle(decoded.ToArray(), decoded.Count - 4);
					decoded.RemoveRange(decoded.Count - 4, 4);
				}
				else if (feature.Type == typeof(string))
				{
					var encoding = new System.Text.ASCIIEncoding();

					var tmpBytes = decoded.GetRange(decoded.Count - feature.Size, feature.Size);
					decoded.RemoveRange(decoded.Count - feature.Size, feature.Size);
					var newString = encoding.GetString(tmpBytes.ToArray());
					feature.Value = newString.Replace("\0", "");
				}
				else if (feature.Type == typeof(DateTime))
				{
					feature.Value = new DateTime(BitConverter.ToInt64(decoded.ToArray(), decoded.Count - 8));
					decoded.RemoveRange(decoded.Count - 8, 8);
				}
			}

			for (int i = 0; i < boolFeatures.Length; i += 8)
			{
				byte currentByte = decoded[decoded.Count - 1];
				for (int k = 0; k < 8 && k + i < boolFeatures.Length; ++k)
				{
					boolFeatures[k + i].Value = (currentByte & (byte)(1 << k)) != 0;
				}
				decoded.RemoveAt(decoded.Count - 1);
			}

			return GetByteFeature("_internalFeatureByteCount") == decoded.Count;
		}

		private void FillFeatureBytes(List<byte> bytes)
		{
			var boolFeatures = featureMap.Values.Where(entry => entry.Type == typeof(bool)).ToArray();
			var otherFeatures = featureMap.Values.Where(entry => entry.Type != typeof(bool)).ToArray();

			for (int i = 0; i < boolFeatures.Length; i += 8)
			{
				byte newByte = 0x0;
				for (int k = 0; k < 8 && k + i < boolFeatures.Length; ++k)
				{
					if ((bool)boolFeatures[k + i].Value == true)
						newByte |= (byte)(1 << k);
				}
				bytes.Add(newByte);
			}

			for (int i = 0; i < otherFeatures.Length; ++i)
			{
				var feature = otherFeatures[i];
				byte[] newBytes = null;

				if (feature.Type == typeof(int))
					newBytes = BitConverter.GetBytes((int)feature.Value);
				else if (feature.Type == typeof(byte))
					newBytes = new byte[] { (byte)feature.Value };
				else if (feature.Type == typeof(float))
					newBytes = BitConverter.GetBytes((float)feature.Value);
				else if (feature.Type == typeof(string))
				{
					var encoding = new System.Text.ASCIIEncoding();
					var tmpBytes = encoding.GetBytes((string)feature.Value);

					var byteList = new List<byte>();
					byteList.AddRange(tmpBytes.Take(feature.Size));
					if (byteList.Count < feature.Size)
						byteList.AddRange(new byte[feature.Size - byteList.Count]);

					newBytes = byteList.ToArray();
				}
				else if (feature.Type == typeof(DateTime))
					newBytes = BitConverter.GetBytes(((DateTime)feature.Value).Ticks);

				if (newBytes != null)
					bytes.AddRange(newBytes);
			}

			byte emptyFeaturesToAdd = (byte)Math.Max(0, MinFeatureByteCount - (bytes.Count + 4) - 1);
			bytes.Add(emptyFeaturesToAdd);
			var byteArray = bytes.ToArray();
			bytes.Clear();
			
			var random = new Random();
			for (int i = 0; i < emptyFeaturesToAdd; ++i)
				bytes.Add((byte)(random.Next(1000)));

			bytes.AddRange(byteArray);
		}

		private bool DecodeKeyBytes(List<byte> bytes)
		{
			// lower all bytes
			var bytesArray = bytes.Select((value, index) => (byte)(value - (index + 1) * bytes.Count)).ToArray();
			bytes.Clear();
			bytes.AddRange(bytesArray);

			var saltBytes = new List<byte>();
			saltBytes.Add(bytes[bytes.Count - 3]);
			saltBytes.Add(bytes[bytes.Count - 2]);
			saltBytes.Add(bytes[bytes.Count - 1]);
			var saltBytesArray = saltBytes.ToArray();

			bytes.RemoveRange(bytes.Count - 3, 3);

			// unshake bytes a little
			for (int i = 0; i < 5 + bytes[0]; ++i)
				ByteArrayTools.RotateRight(ref saltBytesArray);

			// unshake salt
			var salt0 = saltBytesArray[2];
			saltBytes = saltBytesArray.ToList();
			saltBytes.RemoveAt(2);

			saltBytesArray = saltBytes.ToArray();
			for (int i = 0; i < salt0 * 3; ++i)
				ByteArrayTools.RotateRight(ref saltBytesArray);
			var salt1 = saltBytesArray[0];
			var salt2 = saltBytesArray[1];

			// unshake bytes
			bytesArray = bytes.ToArray();
			for (int i = 0; i < salt2; ++i)
				ByteArrayTools.RotateRight(ref bytesArray);

			bytes.Clear();
			bytes.AddRange(bytesArray);

			// remove filling bytes
			var extra = (salt0 + 4) % 5;
			if (extra != 0)
				extra = 5 - extra;
			for (int i = 0; i < extra; ++i)
				bytes.RemoveAt(bytes.Count - 1);

			var hashSum = bytes[bytes.Count - 1];
			bytes.RemoveAt(bytes.Count - 1);

			var currentSum = (byte)bytes.Sum(value => value);
			if (hashSum != currentSum)
				return false;

			return true;
		}

		private void EncodeKeyBytes(List<byte> bytes)
		{
			var salt0 = (byte)bytes.Count;

			var hashSum = (byte)bytes.Sum(value => value);
			bytes.Add(hashSum);

			var random = new Random();
			var salt1 = (byte)(random.Next(10 * 1000) + 123);
			var salt2 = (byte)(salt0 * salt1);

			// shake salt
			var saltBytes = new List<byte>();
			saltBytes.Add(salt1);
			saltBytes.Add(salt2);

			var saltBytesArray = saltBytes.ToArray();
			for (int i = 0; i < salt0 * 3; ++i)
				ByteArrayTools.RotateLeft(ref saltBytesArray);
			saltBytes = saltBytesArray.ToList();

			saltBytes.Add(salt0);

			// add filling bytes
			var extra = (salt0 + 4) % 5;
			if (extra != 0)
				extra = 5 - extra;
			for (int i = 0; i < extra; ++i)
				bytes.Add((byte)random.Next(255));

			// shake data + filling bytes
			var bytesArray = bytes.ToArray();
			for (int i = 0; i < salt2; ++i)
				ByteArrayTools.RotateLeft(ref bytesArray);
			
			bytes.Clear();
			bytes.AddRange(bytesArray);

			// shake salt a little bit more
			saltBytesArray = saltBytes.ToArray();
			for (int i = 0; i < 5 + bytesArray[0]; ++i)
				ByteArrayTools.RotateLeft(ref saltBytesArray);

			bytes.AddRange(saltBytesArray);
			bytesArray = bytes.ToArray();
			bytes.Clear();

			// raise all bytes
			bytes.AddRange(bytesArray.Select((value, index) => (byte)(value + (index + 1) * bytesArray.Length)));
		}
	}
}
